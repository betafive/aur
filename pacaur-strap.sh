#! /bin/bash

set -e

sudo pacman -S base-devel

curl https://aur.archlinux.org/packages/co/cower/cower.tar.gz -o cower.tar.gz
tar xzf cower.tar.gz
cd cower
makepkg -si
cd ..

cower -d pacaur
cd pacaur
makepkg -si
cd ..
